<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination constants
    |--------------------------------------------------------------------------
    |
    | These values define the pagination related parameters. These value are used when the
    | APIs are returning collection of records.
    |
     */

    'pageSize' => 25,

    /*
    |--------------------------------------------------------------------------
    | Search Radius
    |--------------------------------------------------------------------------
    |
    | This value defines the search radius (km) for near by entities. This value is used when
    | APIs are returning collection of entities near the given location.
    |
     */

    'searchRadius' => 5,

];

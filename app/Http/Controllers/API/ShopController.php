<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\Shop as ShopResource;
use App\Http\Resources\ShopCollection as ShopResourceCollection;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pageSize = Config::get('api.pageSize');
        if ($request->query('pageSize') != null) {
            $pageSize = $request->query('pageSize');
        }
        return new ShopResourceCollection(Shop::paginate($pageSize));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'owner_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()],
                Response::HTTP_BAD_REQUEST);
        }
        $input = $request->all();
        $shop = '';

        try {
            $shop = Shop::create($input);
        } catch (\Exception $e) {
            report($e);
            return response('', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['data' => $shop], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        return new ShopResource($shop);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shop $shop)
    {
        // Is it required ??
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shop $shop)
    {
        $shop->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Return NearBy shops.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function nearby(Request $request)
    {
        $searchRadius = Config::get('api.searchRadius');
        $pageSize = Config::get('api.pageSize');
        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'lng' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()],
                Response::HTTP_BAD_REQUEST);
        }
        if ($request->radius) {
            $searchRadius = $request->radius;
        }
        if ($request->pageSize) {
            $pageSize = $request->pageSize;
        }

        $haversine = "(6371 * acos(cos(radians($request->lat))
        * cos(radians(lat))
        * cos(radians(lng)
        - radians($request->lng))
        + sin(radians($request->lat))
        * sin(radians(lat))))";

        $nearByShops = Shop::select(['id', 'name', 'address'])
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$searchRadius])
            ->orderBy('distance', 'asc')
            ->paginate($pageSize);

        return new ShopResourceCollection($nearByShops);

    }
}
